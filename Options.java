import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Options {

	public static void clone(Path path) {
		String gitClone, vanilla, tbc, wotlk, cata, mop, wod, legion = "";
		List<String> list = new ArrayList<>();
		gitClone = "git clone ";
		// vanilla = "https://github.com/LightsHope/server.git";
		tbc = "https://github.com/OregonCore/OregonCore.git";
		wotlk = "-b 3.3.5 https://github.com/TrinityCore/TrinityCore/";
		cata = "https://gitlab.com/trinitycore/TrinityCore_434.git";
		mop = "https://github.com/ProjectSkyfire/SkyFire.548.git";
		wod = "-b 6.x https://github.com/TrinityCoreLegacy/TrinityCore.git";
		legion = "https://github.com/TrinityCore/TrinityCore.git";
		// String[] versions = { vanilla, tbc, wotlk, cata, mop, wod, legion };
		// list.add(vanilla);
		list.add(tbc);
		list.add(wotlk);
		list.add(cata);
		list.add(mop);
		list.add(wod);
		list.add(legion);
		String toDo = "cd " + path + "; mkdir Cores; cd Cores";
		// executeCommand("mkdir" + " " + path + "/Cores");
		// executeCommand("cd Cores");
		// for (int i = 0; i <= 6; i++) {
		// executeCommand(gitClone + versions[i]);
		// optional:
		// renameDirsToVersion();
		// example: TrinityCore -> 3.3.5
		// }
		Executor.execute(toDo);
		// list.forEach(core -> executeCommand(gitClone + core));
		list.forEach(core -> Executor.execute(gitClone + core));

	}
}
