import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

public class WoWCores {

	public static void main(String[] args) {

		if (args.length > 0) {
			if (args[0].equals("-clone")) {
				Path path = Paths.get(args[1]);
				Options.clone(path);
			} else if (args[0].equals("-update")) {
				Path path = Paths.get(args[1]);
				// update(path);
			} else if (args[0].equals("-help")) {
				help();
			} else {
				wrongSyntax();
			}
		} else {
			wrongSyntax();
		}
	}

	public static void renameDirsToVersion() {

	}

	public static void help() {

		System.out.println(
				"WoWCores Options: \n 1.- WoWCores -clone pathToClone \n Example: WoWCores -clone /home/myUser/ \n The program will create a directory named WoWCores into which will be all the cores. \n 2.- WoWCores -update pathToUpdate \n Example: WoWCores -update /home/myUser/ \n The program will automatically update all the cores to the last version. \n\n I'm working on a update option to update only a selected core.");

	}

	public static void wrongSyntax() {
		System.out.println("Wrong Syntax: WoWCores -option /path/to/cores/ \n Or try WoWCores -help");
	}
}
